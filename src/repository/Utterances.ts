export const utterances = [
    {
        'text': 'order a pizza',
        'intentName': 'ModifyOrder',
        'entityLabels': [
            {
                'entityName': 'Order',
                'startCharIndex': 6,
                'endCharIndex': 12
            }
        ]
    },
    {
        'text': 'order a large pepperoni pizza',
        'intentName': 'ModifyOrder',
        'entityLabels': [
            {
                'entityName': 'Order',
                'startCharIndex': 6,
                'endCharIndex': 28
            },
            {
                'entityName': 'FullPizzaWithModifiers',
                'startCharIndex': 6,
                'endCharIndex': 28
            },
            {
                'entityName': 'PizzaType',
                'startCharIndex': 14,
                'endCharIndex': 28
            },
            {
                'entityName': 'Size',
                'startCharIndex': 8,
                'endCharIndex': 12
            }
        ]
    },
    {
        'text': 'I want two large pepperoni pizzas on thin crust',
        'intentName': 'ModifyOrder',
        'entityLabels': [
            {
                'entityName': 'Order',
                'startCharIndex': 7,
                'endCharIndex': 46
            },
            {
                'entityName': 'FullPizzaWithModifiers',
                'startCharIndex': 7,
                'endCharIndex': 46
            },
            {
                'entityName': 'PizzaType',
                'startCharIndex': 17,
                'endCharIndex': 32
            },
            {
                'entityName': 'Size',
                'startCharIndex': 11,
                'endCharIndex': 15
            },
            {
                'entityName': 'Quantity',
                'startCharIndex': 7,
                'endCharIndex': 9
            },
            {
                'entityName': 'Crust',
                'startCharIndex': 37,
                'endCharIndex': 46
            }
        ]
    }
];