//
// This quickstart shows how to add utterances to a LUIS model using the REST APIs.
//

const request = require('request-promise');

//////////
// Values to modify.

// YOUR-APP-ID: The App ID GUID found on the www.luis.ai Application Settings page.
const LUIS_appId = "303aad40-bfae-4ab1-a774-079ca9487853";

// YOUR-AUTHORING-KEY: Your LUIS authoring key, 32 character value.
const LUIS_authoringKey = "8fd8b1f64c08485cb7cd250c5cd5a739";

// YOUR-AUTHORING-ENDPOINT: Replace this with your authoring key endpoint.
// For example, "https://your-resource-name.cognitiveservices.azure.com/"
const LUIS_endpoint = "https://pizza-resource.cognitiveservices.azure.com/";

// NOTE: Replace this your version number. The Pizza app uses a version number of "0.1".
const LUIS_versionId = "0.1";
//////////

const addUtterancesURI = `${LUIS_endpoint}luis/authoring/v3.0-preview/apps/${LUIS_appId}/versions/${LUIS_versionId}/examples`;
const addTrainURI = `${LUIS_endpoint}luis/authoring/v3.0-preview/apps/${LUIS_appId}/versions/${LUIS_versionId}/train`;

// Adds the utterances to the model.
export const addUtterances = async (utterances) => {

    const options = {
        uri: addUtterancesURI,
        method: 'POST',
        headers: {
            'Ocp-Apim-Subscription-Key': LUIS_authoringKey
        },
        json: true,
        body: utterances
    };

    const response = await request(options);
    console.log("addUtterance:\n" + JSON.stringify(response, null, 2));
};

// With verb === "POST", sends a training request.
// With verb === "GET", obtains the training status.
export const train = async (verb) => {

    const options = {
        uri: addTrainURI,
        method: verb,
        headers: {
            'Ocp-Apim-Subscription-Key': LUIS_authoringKey
        },
        json: true,
        body: null // The body can be empty for a training request
    };

    const response = await request(options);
    console.log("train " + verb + ":\n" + JSON.stringify(response, null, 2));
};