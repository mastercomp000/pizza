import {getPrediction} from "../services/PredictService";
import {train, addUtterances} from "../services/ModelService";

export class MyController {
    invoke = async (utterances: object, methods: string | string[], checkPrediction: boolean = true) => {
        if (checkPrediction) await getPrediction();
        await addUtterances(utterances);

        if (typeof methods === 'string') {
            await train(methods);
        } else {
            await methods.forEach(method => {
                train(method);
            })
        }
    };
}