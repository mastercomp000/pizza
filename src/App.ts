import {utterances} from "./repository/Utterances";
import {MyController} from "./controllers/Controller";
let controller = new MyController();


controller.invoke(utterances, ['POST', 'GET'])
    .then(() => console.log("done"))
    .catch((err) => console.log(err));